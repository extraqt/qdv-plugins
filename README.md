QDocumentView Plugins
=====================

This repo contains a few plugins for QDocumentView library.
The following plugins are supported/planned:

- [ ] Comicbook (cbz, cbr, c7z)
- [ ] Compressed HTML files (chm)  
- [ ] DVI files
- [ ] Epub
- [ ] Fax
- [ ] Fictionbook (fb2)  
- [ ] Kimgio  
- [ ] Markdown (md)  
- [ ] Mobipocket  plucker  tiff  txt  xps
- [ ] Plucker
- [ ] Tiff
- [ ] Text (all text/* formats with syntax highlighting)
- [ ] XPS

Initially, most of the plugins will simply be forked from Okular's sources and modified suitably to work with QDV.
